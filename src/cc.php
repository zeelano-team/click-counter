<?php

/**
 * MSCUS::click-counter
 *
 * (c) 2016 Michael Müller
 */

//INITIALIZE MSCUS
$mscus["version"]       =   0.2;
$mscus["identifier"]    =   "development"; //to identify THIS entity of MSCUS
$mscus["debug"]         =   true;

//include debugging functions
if ($mscus["debug"]) {
    include("debug/pretty_var_dump.php");
}

include ("inc/get_browser_name.php");
include ("inc/get_client_ip.php");
include ("inc/get_client_location.php");
include ("inc/random_string.php");
include ("inc/get_tracking_cookie.php");

//CONFIGURATION
//mysql
$mscus["mysql"]["host"]             = "178.254.6.70";
$mscus["mysql"]["username"]         = "mscus_test_user";
$mscus["mysql"]["password"]         = "DHw3bW47K3ChVLW9";
$mscus["mysql"]["database_name"]    = "mscus_testing";
$mscus["mysql"]["port"]             = 3306;

//information to collect
$mscus["config"] = array(
    "server_ip"         =>  true,   //123.123.123.123
    "server_name"       =>  true,   //i.e. domain name
    "request_scheme"    =>  true,   //http vs. https
    "request_method"    =>  true,   //post vs. get
    "request_uri"      =>  true,    //dir/dir/index.php?u=23
    "http_user_agent"   =>  true,
    "remote_address"    =>  true,
    "user_location"     =>  true,
    "tracking_cookie"   =>  13  //number of characters in cookie, or false if disabled. default: 13, max: 20
);

//TEST CONFIGURATION
if (!function_exists("mysqli_connect")) {
    die ("MySQL was not found on this server. You might have to enable it in your php.ini");
}
$mscus["db"] = new mysqli(
    $mscus["mysql"]["host"],
    $mscus["mysql"]["username"],
    $mscus["mysql"]["password"],
    $mscus["mysql"]["database_name"],
    $mscus["mysql"]["port"]
);

//checking connection
if ($mscus["db"]->connect_errno) {
    die ("Failed to connect to MySQL: " . $mscus["db"]->connect_errno);
}

//checking if clicks table exists
$query = "SHOW TABLES LIKE 'clicks'";
$result = $mscus["db"]->query($query);

if (!$result || $result->num_rows==0) {
    //no table found, create it

    $query = "CREATE TABLE `clicks` (
 `click_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
 `server_ip` int(10) unsigned DEFAULT NULL,
 `server_name` varchar(128) DEFAULT NULL,
 `request_scheme` varchar(16) DEFAULT NULL,
 `request_method` varchar(16) DEFAULT NULL,
 `request_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
 `request_uri` varchar(512) DEFAULT NULL,
 `http_user_agent` varchar(128) DEFAULT NULL,
 `remote_address` int(10) unsigned DEFAULT NULL,
 `user_location` varchar(128) DEFAULT NULL,
 `tracking_cookie` varchar(20) DEFAULT NULL,
 `identifier` varchar(20) DEFAULT NULL,
 PRIMARY KEY (`click_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1";

    $result = $mscus["db"]->query($query);

    if (!$result) {
        die ("Could not create table.");
    }

    if ($mscus["debug"])
        echo "Table successfully created";

}

//COLLECT INFORMATION
if ($mscus["debug"])
    pretty_var_dump($_SERVER);

//TODO: check, if collected data exists

$mscus["data"] = array(
    "server_ip"         =>  ($mscus["config"]["server_ip"])         ?   ip2long($_SERVER ["SERVER_ADDR"])   : null,
    "server_name"       =>  ($mscus["config"]["server_name"])       ?   $_SERVER ["SERVER_NAME"]   : null,
    "request_scheme"   =>  ($mscus["config"]["request_scheme"])   ?   $_SERVER ["REQUEST_SCHEME"]   : null,
    "request_method"    =>  ($mscus["config"]["request_method"])    ?   $_SERVER ["REQUEST_METHOD"]   : null,
    "request_uri"      =>  ($mscus["config"]["request_uri"])      ?   $_SERVER["REQUEST_URI"]   : null,
    "http_user_agent"   =>  ($mscus["config"]["http_user_agent"])   ?   get_browser_name($_SERVER["HTTP_USER_AGENT"]):null,
    "remote_address"    =>  ($mscus["config"]["remote_address"])    ?   ip2long(get_client_ip())   : null,
    "user_location"     =>  ($mscus["config"]["user_location"])     ?   get_client_location(get_client_ip())   : null,
    "tracking_cookie"   =>  ($mscus["config"]["tracking_cookie"]) ? get_tracking_cookie($mscus["config"]["tracking_cookie"]) : null
);

if ($mscus["debug"])
    pretty_var_dump($mscus["data"]);

//INSERT INFORMATION
//using prepared statements to avoid any sort of abuse through sql injection
$stmt = $mscus["db"]->prepare(<<<MYSQL
INSERT INTO `clicks` (
`server_ip`,
`server_name`,
`request_scheme`,
`request_method`,
`request_uri`,
`http_user_agent`,
`remote_address`,
`user_location`,
`tracking_cookie`,
`identifier`
) VALUES (?,?,?,?,?,?,?,?,?,?);
MYSQL
);

$stmt->bind_param("ssssssssss",
    $mscus["data"]["server_ip"],
    $mscus["data"]["server_name"],
    $mscus["data"]["request_scheme"],
    $mscus["data"]["request_method"],
    $mscus["data"]["request_uri"],
    $mscus["data"]["http_user_agent"],
    $mscus["data"]["remote_address"],
    $mscus["data"]["user_location"],
    $mscus["data"]["tracking_cookie"],
    $mscus["identifier"]
);

$stmt->execute();
$stmt->close();




//close db connection
$mscus["db"]->close();