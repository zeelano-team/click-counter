<?php
function random_string($length=10) {
    $characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $numCharacters = strlen($characters);
    $random_string = "";
    for ($i=0;$i<$length;$i++) {
        $random_string .= $characters[rand(0,$numCharacters-1)];
    }
    return $random_string;
}