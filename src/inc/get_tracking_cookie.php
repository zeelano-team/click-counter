<?php
function get_tracking_cookie($length) {
    if (isset($_COOKIE["visitor"])) {
        $tracking_cookie = $_COOKIE["visitor"];
    } else {
        $tracking_cookie = random_string($length);
    }
    setcookie("visitor", $tracking_cookie, strtotime('+365 days'), "/", false);
    return $tracking_cookie;
}