<?php
function get_client_location($ip, $delimiter=",") {
    $details = json_decode(file_get_contents("https://ipinfo.io/".$ip."/geo"));
    return $details->city . $delimiter . $details->region . $delimiter . $details->country;
}